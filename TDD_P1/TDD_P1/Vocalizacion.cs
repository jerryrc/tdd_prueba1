﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDD_P1
{
    class Vocalizacion
    {
        private string palabra { get; set; }

        public Vocalizacion(string p)
        {
            palabra = p;
        }

        public string TraerPalabra()
        {
            return palabra;
        }

        public int ContarVocales()
        {
            return 0;
        }
        public int Contarconsonantes()
        {
            return 0;
        }
    }
}
