﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDD_P1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Inicializa PruebasUnitarias
            PruebasUnitarias pu = new PruebasUnitarias();

            // Pide y lee entrada.
            Console.WriteLine("Por favor ingrese una palabra para el conteo de sus vocales y consonantes, gracias...\n");
            var palabra = Console.ReadLine();

            // Validación inicial, está vacío o no.
            if (!String.IsNullOrEmpty(palabra))
            {
                // Validaciones secuandarias, es número, tiene caracteres especiales, tiene espacios blancos.
                if (!pu.IsNumber(palabra) && !pu.IsEspChar(palabra) && !pu.HasBlankSpace(palabra))
                {
                    Vocalizacion v = new Vocalizacion(palabra);
                    Console.WriteLine(v.TraerPalabra());
                }
            }
            else Console.WriteLine("No escribió nada... :v");
            Console.ReadKey();
        }
    }
}
