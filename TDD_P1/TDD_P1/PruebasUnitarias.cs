﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TDD_P1
{
    class PruebasUnitarias
    {
        public bool IsNumber(string p)
        {
            if (Regex.IsMatch(p,"[0-9]"))
            {
                Console.WriteLine("Su palabra contiene numeros...");
                return true;
            }
            return false;
        }

        public bool IsEspChar(string p)
        {
            if (Regex.IsMatch(p,"[!\"#$%&/()=?¡¿¨*{}^`|°¬.;,:]"))
            {
                Console.WriteLine("Su palabra contiene caracteres no permitidos...");
                return true;
            }
            return false;
        }

        public bool HasBlankSpace(string p)
        {
            if (Regex.IsMatch(p," "))
            {
                Console.WriteLine("Su palabra contiene espacios...");
                return true;
            }
            return false;
        }
    }
}
